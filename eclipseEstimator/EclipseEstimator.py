import csv
import math
import matplotlib.pyplot as plt

# ()
def eclipseEstimator(period, pulseWidth, forecastLength, offset):
    data = []

    for t in range (forecastLength):
        if t < offset:
            data.append(0)
        elif (t - offset-(math.trunc((t-offset)*(1/period)))/(1/period)) < pulseWidth:
            data.append(1)
        else:
            data.append(0)
    return data

PERIOD = 5698-23
PULSEWIDTH = 2005
FORECASTLENGTH = 500000
OFFSET = 8150

actualData = []
x = []

x_val = 0

with open('./EclipseTrainData_1Week.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    i = 0

    for row in csv_reader:

        if(line_count > 0):
            actualData.insert(i,row[0]) #arr[i] = row
            x.append(x_val)
            x_val = x_val+10
            i += 1
            line_count += 1
        if(line_count == 0):
            line_count += 1

plt.plot(eclipseEstimator(PERIOD,PULSEWIDTH,FORECASTLENGTH, OFFSET))
plt.plot(x,actualData)
plt.show()
