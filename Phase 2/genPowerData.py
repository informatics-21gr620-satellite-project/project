import csv
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import numpy as np
import random

NumOfPeriods = 1
UGLY_CHANCE = 2
REPEATED_UGLY = 50


data = [[]]
X = []

watt_0 = []
watt_1 = []
watt_2 = []
watt_3 = []
watt_4 = []
watt_5 = []
total = []
timestamp = []

with open('dataReal.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    i = 0

    for row in csv_reader:

        if(line_count > 0):
            data.insert(i,row) #arr[i] = row
            i += 1
            line_count += 1
        if(line_count == 0):
            line_count += 1

for j in range(line_count-1):


    watt_0.insert(j,(int(data[j][13])*0.001) * (int(data[j][8])*0.001))
    watt_1.insert(j,(int(data[j][4])/1000) * (int(data[j][12])/1000))
    watt_2.insert(j,(int(data[j][14])/1000) * (int(data[j][3])/1000))
    watt_3.insert(j,(int(data[j][5])/1000) * (int(data[j][6])/1000))
    watt_4.insert(j,(int(data[j][18])/1000) * (int(data[j][17])/1000))
    watt_5.insert(j,(int(data[j][1])/1000) * (int(data[j][2])/1000))
    total.append(watt_0[j]+watt_1[j]+watt_2[j]+watt_3[j]+watt_4[j]+watt_5[j])
    timestamp.insert(j, int(data[j][0]))



dates = [dt.datetime.fromtimestamp(ts) for ts in timestamp]

watt_0 = np.array(watt_0)
watt_1 = np.array(watt_1)
watt_2 = np.array(watt_2)
watt_3 = np.array(watt_3)
watt_4 = np.array(watt_4)
watt_5 = np.array(watt_5)
watt_0_interp = []
watt_1_interp = []
watt_2_interp = []
watt_3_interp = []
watt_4_interp = []
watt_5_interp = []
total = np.array(total)
totalInterp = []

for i in range(1539283870, 1539369030, 10):
    totalInterp.append(np.interp(i, timestamp, total))
    watt_0_interp.append(np.interp(i, timestamp, watt_0))
    watt_1_interp.append(np.interp(i, timestamp, watt_1))
    watt_2_interp.append(np.interp(i, timestamp, watt_2))
    watt_3_interp.append(np.interp(i, timestamp, watt_3))
    watt_4_interp.append(np.interp(i, timestamp, watt_4))
    watt_5_interp.append(np.interp(i, timestamp, watt_5))
    X.append(i)

arrs = []
for i in range(0,8516, 568):
    temp = []
    for j in range(567):
        index = i+j
        if(index > 8515):
            break
        temp.append([watt_0_interp[index], watt_1_interp[index], watt_2_interp[index], watt_3_interp[index], watt_4_interp[index], watt_5_interp[index]])

    arrs.append(temp)



arrs.pop(14)
arrs.pop(13)
arrs.pop(6)
arrs.pop(5)


def buildDataRand(numPeriods):
    data = []

    for period in range(numPeriods):
        randPeriod = arrs[random.randint(0,10)]

        for i in range(len(randPeriod)):
            data.append(randPeriod[i])

    return data

def buildDataSpecific(first, second, numPeriods):
    data = []
    print(first, second)
    for periods in range(numPeriods):
        specificPeriod1 = arrs[first]
        specificPeriod2 = arrs[second]

        for i in range(len(specificPeriod1)):
            data.append(specificPeriod1[i])
        for i in range(len(specificPeriod2)):
            data.append(specificPeriod2[i])

    return data


def dataNoisyfier(data, chance, repeatedChance):
    # adds errors to the data in the form of drops in values of the channels
    # the errors are added at some frequency specified in function parameters

    originalChance = chance

    for column in range(len(data[0])):
        for row in range(len(data)):
            if random.randint(0,1000) < chance:

                data[row][column] = 0
                if random.randint(0,1000) < repeatedChance:
                    chance = 1001
                else:
                    chance = originalChance
    return data


def createTestSets():
    generatedData = []
    counter = 0
    for i in range(len(arrs)):
        for j in range(len(arrs)):
            if j != i:

                generatedData = buildDataSpecific(i,j,NumOfPeriods)

                X = []
                for ii in range(len(generatedData)):
                    X.append(ii)

                Xdates = [dt.datetime.fromtimestamp(ts) for ts in X]

                data = pd.DataFrame(data=generatedData, columns=("chan0", "chan1", "chan2", "chan3", "chan4", "chan5"))

                d = {'timestamp': Xdates, 'Channel0': data["chan0"], 'Channel1': data["chan1"], 'Channel2': data["chan2"], 'Channel3': data["chan3"], 'Channel4': data["chan4"], 'Channel5': data["chan5"]}

                toWrite = pd.DataFrame(data=d)

                toWrite.to_csv('./TestSets/TestSet_%s.csv' % counter, index=False)

                counter += 1


def createNoisyTestSets():

    counter = 0
    for i in range(10):


        generatedData = dataNoisyfier(buildDataRand(NumOfPeriods), 10, 980)
        cleanData = buildDataRand(NumOfPeriods)

        for j in range(len(cleanData)):
            generatedData.append(cleanData[j])
        X = []
        for ii in range(len(generatedData)):
            X.append(ii)

        Xdates = [dt.datetime.fromtimestamp(ts) for ts in X]

        data = pd.DataFrame(data=generatedData, columns=("chan0", "chan1", "chan2", "chan3", "chan4", "chan5"))

        d = {'timestamp': Xdates, 'Channel0': data["chan0"], 'Channel1': data["chan1"], 'Channel2': data["chan2"], 'Channel3': data["chan3"], 'Channel4': data["chan4"], 'Channel5': data["chan5"]}

        toWrite = pd.DataFrame(data=d)

        toWrite.to_csv('./NoisyTestSets/TestSetNoisy_%s.csv' % counter, index=False)
        counter +=1

        dates2 = [dt.datetime.fromtimestamp(ts) for ts in X]

        plt.plot(X,generatedData)
        plt.show()

#createNoisyTestSets()
createTestSets()
