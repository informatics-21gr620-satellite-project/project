import numpy as np
import pandas as pd
from pandas import DataFrame
import torch #pytorch
import torch.nn as nn
from torch.autograd import Variable
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import os

class Parameter:
    """
    Class for a parameter, giving it 2 attributes:
        - name
        - default value
    """

    def __init__(self, name, default, sequence = []):
        self.name = name
        self.default = default
        self.sequence = sequence


#----------- Global variables -----------#
#Constant
INPUT_SIZE =  Parameter("INPUT_SIZE", 6)
OUTPUT_SIZE =  Parameter("OUTPUT_SIZE", 6)
NUM_TEST_SETS = Parameter("NUM_TEST_SETS", 110)
NUM_TEST_SETS_NOISY = Parameter("NUM_TEST_SETS_NOISY", 10)

#able
BATCH_SIZE =  Parameter("BATCH_SIZE", 32)
LEARNING_RATE =  Parameter("LEARNING_RATE", pow(10,-5))
MOMENTUM = Parameter("MOMENTUM", 0.9)
DROPOUT_RATE =  Parameter("DROPOUT_RATE", 0.3)
NUM_LAYERS =  Parameter("NUM_LAYERS", 3)
ACTIVATION_FUNCTION = Parameter("ACTIVATION_FUNCTION", "Sigmoid")
SEQUENCE_LENGTH = Parameter("SEQUENCE_LENGTH", 210)
HIDDEN_SIZE = Parameter("HIDDEN_SIZE", 25)
DATASET = Parameter("DATASET", "small")
OPTIMIZER = Parameter("OPTIMIZER", "SGD")
NUM_EPOCHS = Parameter("NUM_EPOCHS", 50)



# ---------- Class of Changeable Model Parameters ---------- #
class Performance:

    def __init__(self, trainLoss = 0, testLoss = 0, genLoss = 0):
        self.trainLoss = trainLoss
        self.testLoss = testLoss
        self.genLoss = genLoss

class ModelParams:
    """
    Class of all LSTM model parameters
    """
    def __init__(self):

        self.BatchSize = BATCH_SIZE
        self.learningRate = LEARNING_RATE
        self.dropoutRate = DROPOUT_RATE
        self.numLayers = NUM_LAYERS
        self.inputSize = INPUT_SIZE
        self.outputSize = OUTPUT_SIZE
        self.actFnc = ACTIVATION_FUNCTION
        self.momentum = MOMENTUM
        self.sequenceLength = SEQUENCE_LENGTH
        self.dataset = DATASET
        self.hiddenSize = HIDDEN_SIZE
        self.optimizer = OPTIMIZER
        self.numEpochs = NUM_EPOCHS
        self.performance = Performance()

#----------- Define model structure -----------#
class LSTM(nn.Module):
    """
    PyTorch LSTM class
    """
    # General LSTM network architecture
    def __init__(self, outputSize, inputSize, hiddenSize, numLayers, seqLength, dRate, actFnc):
        super().__init__()
        self.hiddenLayerSize = hiddenSize # create an attribute called hidden_layer_size
        self.numOfLayers = numLayers
        self.seqLength = seqLength
        self.lstm = nn.LSTM(input_size=inputSize, hidden_size=hiddenSize, num_layers=numLayers, batch_first=True) # create an attribute called lstm, which takes the value of an LSTM layer.
        self.dropout = nn.Dropout(p=dRate)
        self.linear = nn.Linear(hiddenSize, outputSize) # create an attribute called linear which takes the value of a linear layer.

        # Set activation function according to given argument
        if(actFnc == "ReLU"):
            self.activation = nn.ReLU()
        elif(actFnc == "Tanh"):
            self.activation = nn.Tanh()
        elif(actFnc == "Sigmoid"):
            self.activation = nn.Sigmoid()

    # Forward pass network architecture
    def forward(self, inputSeq, device):
        h0 = Variable(torch.zeros(self.numOfLayers, inputSeq.shape[0], self.hiddenLayerSize)).to(device) #hidden state
        c0 = Variable(torch.zeros(self.numOfLayers, inputSeq.shape[0], self.hiddenLayerSize)).to(device) #internal state
        # Propagate input through LSTM
        inputSeq = inputSeq.to(device)
        output, (hn, cn) = self.lstm(inputSeq, (h0, c0))
        hn = self.dropout(hn)
        hn = hn.view(-1, self.hiddenLayerSize) #Reshaping LSTM output for the Dense layer
        out = self.activation(hn)
        out = self.linear(out)
        if(out.shape[0] == NUM_LAYERS.default):
            return out[-1:]
        else:
            return out[-BATCH_SIZE.default:]




#----------- Prepare model and data -----------#
def prepareData(numTestSets):
    testSets = []

    for i in range(0, numTestSets):
        testSets.append(pd.read_csv("./TestSets/TestSet_%s.csv" % str(i), index_col='timestamp', parse_dates=True))
    return testSets

def prepareDataUgly():
    testSets = []

    for i in range(0, NUM_TEST_SETS_NOISY.default):
        testSets.append(pd.read_csv("./NoisyTestSets/TestSetNoisy_%s.csv" % str(i), index_col='timestamp', parse_dates=True))
    return testSets

def prepareModel(modelParams):
    model = LSTM(modelParams.outputSize.default, modelParams.inputSize.default,
             modelParams.hiddenSize.default, modelParams.numLayers.default,
             modelParams.BatchSize.default, modelParams.dropoutRate.default,
             modelParams.actFnc.default).to(device)
    modelName = modelParams.optimizer.default + "_" + str(modelParams.numEpochs.default)
    model.load_state_dict(torch.load("./Models/%s.pth" % modelName, map_location = torch.device('cpu')))

    lossFunction = nn.MSELoss()
    return model, lossFunction





#----------- Test model -----------#
def testModel(model, testSet, modelParams, lossFnc, device):
    ss = StandardScaler()
    if(ACTIVATION_FUNCTION.default == "Tanh"):
        mms = MinMaxScaler(feature_range=(-1,1))
    else:
        mms = MinMaxScaler()
    totalLoss = 0
    counter = 0
    losses = []
    model.eval()
    start = 570
    testSetLoss = 0
    testSS = ss.fit_transform(testSet)
    forecast = []
    for i in range(start-SEQUENCE_LENGTH.default, start):
        forecast.append(testSS[i])

    for i in range(len(testSS) - start):
        with torch.no_grad():
            sequence = Variable(torch.Tensor(forecast[i : SEQUENCE_LENGTH.default + i][:]))

            targetMMS = mms.fit_transform(testSS[start + i : start + i + 1,:])
            target = Variable(torch.Tensor(targetMMS))
            sequenceFinal = torch.reshape(sequence, (1, sequence.shape[0], sequence.shape[1]))
            # Predict and compute loss
            prediction = model(sequenceFinal, device)
            testSetLoss += lossFnc(prediction, target.to(device)).item()
            predictionList = prediction.tolist()
            forecast.append(mms.inverse_transform(predictionList)[0])


    dataRange, forecastRange = [], []
    for i in range(len(testSS)):
        dataRange.append(i)
    for i in range(start, (start - SEQUENCE_LENGTH.default) + len(forecast)):
        forecastRange.append(i)
    test = np.array(ss.inverse_transform(testSS))
    forecastNonSS = np.array(ss.inverse_transform(forecast))
    toWritePred = pd.DataFrame(data=forecastNonSS, columns=("chan0", "chan1", "chan2", "chan3", "chan4", "chan5"))
    toWriteActual = pd.DataFrame(data=test, columns=("chan0", "chan1", "chan2", "chan3", "chan4", "chan5"))
    path = './Predictions/' + str(modelParams.optimizer.default) + "_" + str(modelParams.numEpochs.default) + "/"

    if not os.path.exists(path):
        os.makedirs(path)

    toWritePred.to_csv(path+str(modelParams.optimizer.default) + "_" + str(modelParams.numEpochs.default) + "_" + "prediction_%s.csv" % counter, index=False)
    toWriteActual.to_csv(path+str(modelParams.optimizer.default) + "_" + str(modelParams.numEpochs.default) + "_" + "TestData_%s.csv" % counter, index=False)
    counter += 1

    totalLoss += (testSetLoss / (len(forecast) - SEQUENCE_LENGTH.default))
    losses.append((testSetLoss / (len(forecast) - SEQUENCE_LENGTH.default)))

    averageLoss = (totalLoss / len(testSet))

    losses.append(0)
    losses.append(averageLoss)
    print("Prediction done")
    lossesToCsv = pd.DataFrame(data=losses)
    lossesToCsv.to_csv('./Predictions/' + str(modelParams.optimizer.default) + "_" + str(modelParams.numEpochs.default) + "/losses.csv")
    return toWritePred[-567:]
