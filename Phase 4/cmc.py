"""
CMC (Charging Multiplier Calculator) and battery model
"""
#-Libraries---------------------------------------------------------------------

import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
from Predict import testModel
import torch
import torch.nn as nn
from torch.autograd import Variable

#-Structures and variables -----------------------------------------------------

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print("Using {} device".format(device))

class Parameter:
    """
    Class for a parameter, giving it 2 attributes:
        - name
        - default value
    """

    def __init__(self, name, default, sequence = []):
        self.name = name
        self.default = default
        self.sequence = sequence

#Constant
INPUT_SIZE =  Parameter("INPUT_SIZE", 6)
OUTPUT_SIZE =  Parameter("OUTPUT_SIZE", 6)
NUM_TEST_SETS = Parameter("NUM_TEST_SETS", 110)
NUM_TEST_SETS_NOISY = Parameter("NUM_TEST_SETS_NOISY", 10)

#able
BATCH_SIZE =  Parameter("BATCH_SIZE", 128)
LEARNING_RATE =  Parameter("LEARNING_RATE", pow(10,-6))
MOMENTUM = Parameter("MOMENTUM", 0.9)
DROPOUT_RATE =  Parameter("DROPOUT_RATE", 0.4)
NUM_LAYERS =  Parameter("NUM_LAYERS", 2)
ACTIVATION_FUNCTION = Parameter("ACTIVATION_FUNCTION", "ReLU")
SEQUENCE_LENGTH = Parameter("SEQUENCE_LENGTH", 570)
HIDDEN_SIZE = Parameter("HIDDEN_SIZE", 800)
DATASET = Parameter("DATASET", "small")
OPTIMIZER = Parameter("OPTIMIZER", "SGD")
NUM_EPOCHS = Parameter("NUM_EPOCHS", 50)



class ModelParams:
    """
    Class of all LSTM model parameters
    """
    def __init__(self):

        self.BatchSize = BATCH_SIZE
        self.learningRate = LEARNING_RATE
        self.dropoutRate = DROPOUT_RATE
        self.numLayers = NUM_LAYERS
        self.inputSize = INPUT_SIZE
        self.outputSize = OUTPUT_SIZE
        self.actFnc = ACTIVATION_FUNCTION
        self.momentum = MOMENTUM
        self.sequenceLength = SEQUENCE_LENGTH
        self.dataset = DATASET
        self.hiddenSize = HIDDEN_SIZE
        self.optimizer = OPTIMIZER
        self.numEpochs = NUM_EPOCHS


#----------- Define model structure -----------#
class LSTM(nn.Module):
    """
    PyTorch LSTM class
    """
    # General LSTM network architecture
    def __init__(self, outputSize, inputSize, hiddenSize, numLayers, seqLength, dRate, actFnc):
        super().__init__()
        self.hiddenLayerSize = hiddenSize
        self.numOfLayers = numLayers
        self.seqLength = seqLength
        self.lstm = nn.LSTM(input_size=inputSize, hidden_size=hiddenSize, num_layers=numLayers, batch_first=True)
        self.dropout = nn.Dropout(p=dRate)
        self.linear = nn.Linear(hiddenSize, outputSize)

        # Set activation function according to given argument
        if(actFnc == "ReLU"):
            self.activation = nn.ReLU()
        elif(actFnc == "Tanh"):
            self.activation = nn.Tanh()
        elif(actFnc == "Sigmoid"):
            self.activation = nn.Sigmoid()

    # Forward pass network architecture
    def forward(self, inputSeq, device):
        h0 = Variable(torch.zeros(self.numOfLayers, inputSeq.shape[0], self.hiddenLayerSize)).to(device) #hidden state
        c0 = Variable(torch.zeros(self.numOfLayers, inputSeq.shape[0], self.hiddenLayerSize)).to(device) #internal state
        # Propagate input through LSTM
        inputSeq = inputSeq.to(device)
        output, (hn, cn) = self.lstm(inputSeq, (h0, c0))
        hn = self.dropout(hn)
        hn = hn.view(-1, self.hiddenLayerSize) #Reshaping LSTM output for the Dense layer
        out = self.activation(hn)
        out = self.linear(out)
        if(out.shape[0] == NUM_LAYERS.default):
            return out[-1:]
        else:
            return out[-BATCH_SIZE.default:]




#-MatPlotLib configuration------------------------------------------------------

plt.rcParams['figure.figsize'] = (20.0, 10.0)
plt.rcParams.update({'font.size': 12})
plt.style.use('ggplot')

#-Global variables--------------------------------------------------------------

simTS = 10 #[Hz] sample frequency of data
periodLength = 5670 # rounded down to nearest multiple of 10
simLength = 19*periodLength #[s] length of sunlight period / simulation
sunPeriod = 3670
debugIL = []
LSTMPrediction = []

def prepareModel(modelParams, numb):
    model = LSTM(modelParams.outputSize.default, modelParams.inputSize.default,
             modelParams.hiddenSize.default, modelParams.numLayers.default,
             modelParams.BatchSize.default, modelParams.dropoutRate.default,
             modelParams.actFnc.default).to(device)
    modelName = modelParams.optimizer.default + "_" + str(modelParams.numEpochs.default) + "_" + str(numb)
    model.load_state_dict(torch.load("./Models/%s.pth" % modelName, map_location = torch.device('cpu')))

    lossFunction = nn.MSELoss()
    return model, lossFunction


#-Import data-------------------------------------------------------------------

# Power production and consumption from CSV file to arrays
def readData(period):
    powerIn = [] #[W] array for input power/wattage
    powerOut = [] #[W] power output array
    dataSample = pd.read_csv("./TestSets/TestSet_%s.csv" % period, engine="python")
    nextPeriod = period + 1
    dataNext = pd.read_csv("./TestSets/TestSet_%s.csv" % nextPeriod, engine="python")

    powerOut = dataNext['output_W']
    powerIn = dataNext['generation_W']
    sample = pd.DataFrame()
    sample['Channel0'] = dataSample['Channel0']
    sample['Channel1'] = dataSample['Channel1']
    sample['Channel2'] = dataSample['Channel2']
    sample['Channel3'] = dataSample['Channel3']
    sample['Channel4'] = dataSample['Channel4']
    sample['Channel5'] = dataSample['Channel5']

    sampleNext = pd.DataFrame()
    sampleNext['Channel0'] = dataNext['Channel0']
    sampleNext['Channel1'] = dataNext['Channel1']
    sampleNext['Channel2'] = dataNext['Channel2']
    sampleNext['Channel3'] = dataNext['Channel3']
    sampleNext['Channel4'] = dataNext['Channel4']
    sampleNext['Channel5'] = dataNext['Channel5']

    return powerOut, powerIn, sample, sampleNext
# load file storing SOC-OCV lookup table generated for 25degC
ocv_lookup = pd.read_csv("soc_ocv_25degc.csv", skiprows = 1,
                names =['SOC','OCV','OCV4'],index_col=False,engine='python')

#-BatteryPack class definition--------------------------------------------------

class BatteryPack:
    cell_Rs = 0.075 #[Ohm] Resistance of a cell measured at 25degC after 20s pulse
    cellCapacity = 2.6 #[Ah] Considered capacity of a cell at beggining of life
    OCV = 0 # OCV for 1 cell
    SOC = 0

    # Constructor function
    def __init__(self, serial, parallel, initSOC):

        self.serialCells = serial
        self.parallelCells = parallel
        self.Rs = (self.cell_Rs*self.serialCells)/self.parallelCells
        self.capacity = parallel*self.cellCapacity #[Ah] capacity of battery pack
        self.prev_SOC = initSOC #[0-1] set initial state of charge
        self.OCV = np.interp(initSOC, ocv_lookup.SOC, ocv_lookup.OCV4)
        self.voltage = self.OCV

    # Ideal load calculator
    def calculateIdealLoad(self, goalSOC, initialSOC):
        idealILs = []
        for i in range (int(sunPeriod/simTS)):
            f = (3600*(self.prev_SOC-goalSOC)*self.capacity)/(3670-(i*10))
            idealILs.append(f)

            self.voltage = self.OCV - self.Rs * idealILs[-1]
            self.SOC = self.prev_SOC - (idealILs[-1] * simTS)/(self.capacity * 3600)
            self.prev_SOC = self.SOC

        #calculate ideal current load average
        idealIL = np.average(idealILs)

        #init anew
        self.prev_SOC = initialSOC #[0-1] set initial state of charge
        self.OCV = np.interp(initialSOC, ocv_lookup.SOC, ocv_lookup.OCV4)
        self.voltage = self.OCV

        return idealIL

    def calculateCM(self, timeCounter, predictedMaxLoad, idealLoad):

        CMC = idealLoad/((predictedMaxLoad/self.voltage)/self.parallelCells)
        if(CMC > 1):
            CMC = 1
        if(CMC < 0):
            CMC = 1


        return CMC

    def charge(self, input, timeStep, t, isBaseline):
        if isBaseline == True:
            IL = (input/self.voltage)/self.parallelCells
            self.OCV = np.interp(self.prev_SOC, ocv_lookup.SOC, ocv_lookup.OCV4)

            if (self.prev_SOC - (IL * timeStep)/(self.capacity * 3600)) > 0.85:
                IL = (3600*(-0.85+self.prev_SOC)*self.capacity)/timeStep # calculate IL if SOC should be 1
                self.SOC = 0.85
            else:
                self.SOC = self.prev_SOC - (IL * timeStep)/(self.capacity * 3600)


            self.voltage = self.OCV - self.Rs * IL
            self.prev_SOC = self.SOC
            debugIL.append(IL)

        else:
            IL = (input/self.voltage)/self.parallelCells
            self.OCV = np.interp(self.prev_SOC, ocv_lookup.SOC, ocv_lookup.OCV4)
            self.voltage = self.OCV - self.Rs * IL
            self.SOC = self.prev_SOC - (IL * timeStep)/(self.capacity * 3600)
            self.prev_SOC = self.SOC
            debugIL.append(IL)


def predictionCharging():
    modelParams = ModelParams()

    model, lossFnc = prepareModel(modelParams, 2)

    outdata_time = [] # Output array for timekeeping
    outdata_soc = [] # State of charge output array
    cmc_out = []
    counter = 0
    battery = BatteryPack(4,2,0.75)

    for globalTime in range(0, simLength, periodLength):
        print(counter)
        powerOut, powerIn, sample, sampleNext = readData(counter)

        sample = sample.append(sampleNext, ignore_index=True)

        prediction = testModel(model, sample, modelParams, lossFnc, device)

        combinedPrediction = prediction['chan0'] + prediction['chan1'] + prediction['chan2'] + prediction['chan3'] + prediction['chan4'] + prediction['chan5']

        LSTMPrediction = combinedPrediction.values.tolist()
        currentLoad = battery.calculateIdealLoad(0.85, battery.prev_SOC)
        print("\nIdeal load: %f" % currentLoad)
        for i in range(int(periodLength/simTS)):
            predictedInput = powerOut[i] - LSTMPrediction[i]

            CMC = battery.calculateCM((i + int(globalTime/simTS)), predictedInput, currentLoad)
            cmc_out.append(CMC)

            realInput = (powerOut[i] - powerIn[i])*CMC
            print("actual load: " , realInput)
            if(i < sunPeriod/10):
                battery.charge(realInput, simTS, i + int(globalTime/simTS), False)
                outdata_time.append(i * simTS + globalTime)
                outdata_soc.append(battery.SOC)
            else:
                realInput = powerOut[i] - powerIn[i]
                battery.charge(realInput, simTS, i + int(globalTime/simTS), False)
                outdata_time.append(i * simTS + globalTime)
                outdata_soc.append(battery.SOC)
        counter += 1
    #-Plot output-------------------------------------------------------------------
    plt.hlines(0.85, 0, len(outdata_time)*simTS, linestyles='dashed', label='SOC limit')
    plt.plot(outdata_time, outdata_soc, label='SOC [%]')
    plt.plot(outdata_time, debugIL, label='IL [A]')
    plt.plot(outdata_time, cmc_out, 'y', label='CMC')
    plt.xlabel('Time')
    plt.legend()
    plt.show()


def baseLineCharging():
    outdata_time = [] # Output array for timekeeping
    outdata_soc = [] # State of charge output array
    cmc_out = []
    counter = 0
    battery = BatteryPack(4,2,0.75)

    for globalTime in range(0, simLength, periodLength):
        print(counter)
        powerOut, powerIn, sample, sampleNext = readData(counter)

        currentLoad = battery.calculateIdealLoad(0.85, battery.prev_SOC)
        print("\nIdeal load: %f" % currentLoad)
        for i in range(int(periodLength/simTS)):
            input = powerOut[i] - powerIn[i]

            battery.charge(input, simTS, i + int(globalTime/simTS), True)
            outdata_time.append(i * simTS + globalTime)
            outdata_soc.append(battery.SOC)
        counter += 1
    #-Plot output-------------------------------------------------------------------
    plt.hlines(0.85, 0, len(outdata_time)*simTS, linestyles='dashed', label='SOC limit')

    plt.plot(outdata_time, outdata_soc, label='SOC [%]')
    plt.plot(outdata_time, debugIL, label='IL [A]')
    #plt.plot(outdata_time, cmc_out, 'y', label='CMC')
    plt.xlabel('Time')
    plt.legend(loc="lower right")
    plt.show()

def perfectCharging():

    outdata_time = [] # Output array for timekeeping
    outdata_soc = [] # State of charge output array
    cmc_out = []
    counter = 0
    battery = BatteryPack(4,2,0.75)

    for globalTime in range(0, simLength, periodLength):
        powerOut, powerIn, sample, sampleNext = readData(counter)

        currentLoad = battery.calculateIdealLoad(0.85, battery.prev_SOC)
        print("\nIdeal load: %f" % currentLoad)
        for i in range(int(periodLength/simTS)):
            predictedInput = powerOut[i] - powerIn[i]

            CMC = battery.calculateCM((i + int(globalTime/simTS)), predictedInput, currentLoad)
            cmc_out.append(CMC)

            realInput = (powerOut[i] - powerIn[i])*CMC
            print("actual load: " , realInput)
            if(i < sunPeriod/10):
                battery.charge(realInput, simTS, i + int(globalTime/simTS), False)
                outdata_time.append(i * simTS + globalTime)
                outdata_soc.append(battery.SOC)
            else:
                realInput = powerOut[i] - powerIn[i]
                battery.charge(realInput, simTS, i + int(globalTime/simTS), False)
                outdata_time.append(i * simTS + globalTime)
                outdata_soc.append(battery.SOC)
        counter += 1
    #-Plot output-------------------------------------------------------------------
    plt.hlines(0.85, 0, len(outdata_time)*simTS, linestyles='dashed', label='SOC limit')
    plt.plot(outdata_time, outdata_soc, label='SOC [%]')
    plt.plot(outdata_time, debugIL, label='IL [A]')
    #plt.plot(outdata_time, cmc_out, 'y', label='CMC')
    plt.xlabel('Time')
    plt.legend()
    plt.show()

#-Main--------------------------------------------------------------------------
baseLineCharging()
