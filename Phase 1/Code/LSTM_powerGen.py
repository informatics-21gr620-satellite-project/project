import numpy as np
import pandas as pd
from pandas import DataFrame
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import torch #pytorch
import torch.nn as nn
from torch.autograd import Variable
import matplotlib.pyplot as plt
import datetime as dt
from varname import nameof
import csv
import time
import random
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

TEST_SETS = 3
MAX_ITERATIONS  = 1

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print("Using {} device".format(device))

# ---------- Parameter Class ---------- #
class Parameter:
    """
    Class for a parameter, giving it 3 attributes:
        - name
        - default value
        - The sequence to iterate through when modifying the parameter.
    """

    def __init__(self, name, default, sequence = []):
        self.name = name
        self.default = default
        self.sequence =  sequence

# Parameter objects

#Constant
INPUT_SIZE =  Parameter("INPUT_SIZE", 6)
OUTPUT_SIZE =  Parameter("OUTPUT_SIZE", 6)

# Modifiable
BATCH_SIZE =  Parameter("BATCH_SIZE", 32, [32, 64, 128])
LEARNING_RATE =  Parameter("LEARNING_RATE", 0.1, [pow(10,-6), pow(10,-5), pow(10,-4), pow(10,-3), pow(10,-2), 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9])
MOMENTUM = Parameter("MOMENTUM", 0.9, [0.5, 0.9, 0.99])
DROPOUT_RATE =  Parameter("DROPOUT_RATE", 0.5, [0.2, 0.3, 0.4, 0.5])
NUM_LAYERS =  Parameter("NUM_LAYERS", 1, [1,2,3,4])
ACTIVATION_FUNCTION = Parameter("ACTIVATION_FUNCTION", "ReLU", ["ReLU", "Tanh", "Sigmoid"])
SEQUENCE_LENGTH = Parameter("SEQUENCE_LENGTH", 210, [210, 570])
DATASET = Parameter("DATASET", "small", ["small", "medium", "large"])
HIDDEN_SIZE = Parameter("HIDDEN_SIZE", 25, [25, 50, 100, 200, 400, 800]) #

# ---------- Class of Changeable Model Parameters ---------- #
class Performance:

    def __init__(self, trainLoss = 0, testLoss = 0, genLoss = 0):
        self.trainLoss = trainLoss
        self.testLoss = testLoss
        self.genLoss = genLoss

class ModelParams:
    """
    Class of all LSTM model parameters
    """
    BatchSize = BATCH_SIZE
    learningRate = LEARNING_RATE
    dropoutRate = DROPOUT_RATE
    numLayers = NUM_LAYERS
    inputSize = INPUT_SIZE
    outputSize = OUTPUT_SIZE
    actFnc = ACTIVATION_FUNCTION
    momentum = MOMENTUM
    sequenceLength = SEQUENCE_LENGTH
    dataSet = DATASET
    hiddenSize = HIDDEN_SIZE
    performance = Performance()

    def __init__ (self, optimizerType, epochs):

        #Set variables according to given optimizer
        if (optimizerType == "Adam"):
            self.optimizer = Parameter("OPTIMIZER", "Adam")
        elif (optimizerType == "SGD"):
            self.optimizer = Parameter("OPTIMIZER", "SGD")

        # Set epoch parameter according to givin epoch
        if(epochs == 10):
            self.numEpochs = Parameter("NUM_EPOCHS", 10)
        elif(epochs == 50):
            self.numEpochs = Parameter("NUM_EPOCHS", 50)
        elif(epochs == 100):
            self.numEpochs = Parameter("NUM_EPOCHS", 100)



# ---------- Pytorch classes ---------- #
class LSTM(nn.Module):
    """
    PyTorch LSTM class
    """
    # General LSTM network architecture
    def __init__(self, outputSize, inputSize, hiddenSize, numLayers, seqLength, dRate, actFnc):
        super().__init__()
        self.hiddenLayerSize = hiddenSize # create an attribute called hidden_layer_size
        self.numOfLayers = numLayers
        self.seqLength = seqLength
        self.lstm = nn.LSTM(input_size=inputSize, hidden_size=hiddenSize, num_layers=numLayers, batch_first=True) # create an attribute called lstm, which takes the value of an LSTM layer.
        self.dropout = nn.Dropout(p=dRate)
        self.linear = nn.Linear(hiddenSize, outputSize) # create an attribute called linear which takes the value of a linear layer.

        # Set activation function according to given argument
        if(actFnc == "ReLU"):
            self.activation = nn.ReLU()
        elif(actFnc == "Tanh"):
            self.activation = nn.Tanh()
        elif(actFnc == "Sigmoid"):
            self.activation = nn.Sigmoid()

    # Forward pass network architecture
    def forward(self, inputSeq):
        h0 = Variable(torch.zeros(self.numOfLayers, inputSeq.shape[0], self.hiddenLayerSize)).to(device) #hidden state
        c0 = Variable(torch.zeros(self.numOfLayers, inputSeq.shape[0], self.hiddenLayerSize)).to(device) #internal state
        # Propagate input through LSTM
        inputSeq = inputSeq.to(device)
        output, (hn, cn) = self.lstm(inputSeq, (h0, c0))
        hn = self.dropout(hn)
        hn = hn.view(-1, self.hiddenLayerSize) #Reshaping LSTM output for the Dense layer
        out = self.activation(hn)
        out = self.linear(out)
        if(out.shape[0] == NUM_LAYERS.default):
            return out[-1:]
        else:
            return out[-BATCH_SIZE.default:]

class Timeseries(Dataset):
    """
    Class used to convert data into a pyTorch dataset.
    """

    def __init__(self, x, y):
        self.x = Variable(torch.Tensor(x)).to(device)
        self.y = Variable(torch.Tensor(y)).to(device)

    def __getitem__(self, index):
        return (self.x[index], self.y[index])

    def __len__(self):
        return len(self.x)

# --------------------------------------- #

def splitSequences(dataSet, sampleLength):
    """
    Preprocess the data by splitting the data into two list:
        x: array of samples
        y: array of target values

    Sample_i in array x corresponds to target value_i in array y

    Args:
        dataSet(pd.DataFrame): The dataset to be preprocessed
        sampleLength (int): The desired length of a sample
    """

    x, y = list(), list()
    for i in range(len(dataSet) - sampleLength - 1):
        sampleX = np.array(dataSet[i : i+sampleLength, :])
        sampleY = np.array(dataSet[i+sampleLength])
        x.append(sampleX)
        y.append(sampleY)

    return np.array(x), np.array(y)

def scale(data):
    """
    Scale data according to activation function used in the dense layer.
    For tanH scale to range -1 to 1, otherwise 0 to 1 (ReLu and Sigmoid)

    Args:
        data (np.array): Target vectors to be scaled.
    Return:
    """
    if(ACTIVATION_FUNCTION.default == "Tanh"):
        mms = MinMaxScaler(feature_range=(-1,1))
    else:
        mms = MinMaxScaler()

    return mms.fit_transform(data), mms


def trainModel(model, epochs, trainLoader, optim, lossFnc):
    """
        The model trains by forward passing a batch of samples. After each forward pass of a bacth, a backward pass is executed.
        This is repeated for the given amount of epochs.

        Args:
            model(pyTorch Attr): The model to be trained (the model is copied by reference.
                                      Thus, any changes to the model in this function impacts the actual model.)
            epochs(int): The amount of epochs a model will be trained on the entire training set
            optim (pyTorch Attr): The optimizer chosen for the model
            lossFnc(pyTorch Attr):The loss function chosen for the model
    """
    # Training loop
    for i in range(epochs):
        start_time = time.time()
        for batch, target in trainLoader:
            if(batch.shape[0] < BATCH_SIZE.default):
                break
            batch = batch
            optim.zero_grad()
            prediction = model(batch)
            loss = lossFnc(prediction, target)
            loss.backward()
            optim.step()

        # Print
        if i % 1 == 0:
            print("Epoch: %d, loss: %1.5f" % (i, loss.item()), "| Epoch execution time: %1.10f " % (time.time() - start_time), end='\r')
    print('\r')
    return loss.item()

def testModel(model, testSets, lossFnc, ss, mms, doPlot, modelParams):
    """
    Test the model by having it predict forward for a given forecast horizon (len(testSet) - SEQUENCE_LENGTH)
    and compare to the actual test dataset.
    """
    totalLoss = 0
    counter = 1
    for testSet in testSets:

        model.eval()
        start = 570
        # Initiate local variables
        testSetLoss = 0
        testSS = ss.fit_transform(testSet)
        forecast = []
        for i in range(start-SEQUENCE_LENGTH.default, start):
            forecast.append(testSS[i])
        # Forward pass of n batches
        for i in range(len(testSS) - (start)):
            with torch.no_grad():
                # Create target and batch matrix
                sequence = Variable(torch.Tensor(forecast[i : SEQUENCE_LENGTH.default + i][:]))

                targetMMS = mms.fit_transform(testSS[start + i : start + i + 1,:])
                target = Variable(torch.Tensor(targetMMS))
                sequenceFinal = torch.reshape(sequence, (1, sequence.shape[0], sequence.shape[1]))
                # Predict and compute loss
                prediction = model(sequenceFinal)
                testSetLoss += lossFnc(prediction, target.to(device)).item()
                predictionList = prediction.tolist()
                forecast.append(mms.inverse_transform(predictionList)[0])
        # Return average of loss
        if(doPlot == True):
            dataRange, forecastRange = [], []
            for i in range(len(testSS)):
                dataRange.append(i)
            for i in range(start, (start - SEQUENCE_LENGTH.default) + len(forecast)):
                forecastRange.append(i)
            test = ss.inverse_transform(testSS)
            forecastNonSS = ss.inverse_transform(forecast)
            test = np.array(test)
            forecastNonSS = np.array(forecastNonSS)
            toWritePred = pd.DataFrame(data=forecastNonSS, columns=("chan0", "chan1", "chan2", "chan3", "chan4", "chan5"))
            toWriteActual = pd.DataFrame(data=test, columns=("chan0", "chan1", "chan2", "chan3", "chan4", "chan5"))

            toWritePred.to_csv(str(modelParams.optimizer.default) + "_" + str(modelParams.numEpochs.default) + "_" + "prediction_%s.csv" % counter, index=False)
            toWriteActual.to_csv(str(modelParams.optimizer.default) + "_" + str(modelParams.numEpochs.default) + "_" + "TestData_%s.csv" % counter, index=False)
            counter += 1

        totalLoss += (testSetLoss / (len(forecast) - SEQUENCE_LENGTH.default))
    return (totalLoss / len(testSets))


def setModSequence(modelParams):
    """
    Get sequence in which the parameters should be modified.
    There are two sequences as it depends on the optimiser used.

    Args:
        modelParams (ModelParams): Object containing all parameters to be modified.
    Return:
        sequence of model params (list): Predetermined sequence
    """
    if(modelParams.optimizer.default == "SGD"):
        return [modelParams.learningRate, modelParams.BatchSize, modelParams.momentum,
                modelParams.actFnc, modelParams.dropoutRate, modelParams.sequenceLength,
                modelParams.hiddenSize, modelParams.numLayers, modelParams.dataSet,
                modelParams.learningRate, modelParams.BatchSize]

    if(modelParams.optimizer.default == "Adam"):
        return [modelParams.learningRate,modelParams.BatchSize, modelParams.actFnc,
                modelParams.dropoutRate, modelParams.sequenceLength, modelParams.hiddenSize,
                modelParams.numLayers, modelParams.dataSet, modelParams.learningRate, modelParams.BatchSize]

def saveModel(writer, model, modelParams, param, testLoss, isFinal):
    """
    Save a model

    Args:
        writer (writer): the writer to be used to write to file.
        model (pyTorch attr): LSTM model to be saved
    Return:
        Saves a CSV file for 6 different models
    """
    if(isFinal == True):
        pthName =   ("LSTMFinal_"+ "_" + param.name + "_" + str(modelParams.optimizer.default) + "_" +
                    str(modelParams.numEpochs.default) + "_" + str(modelParams.BatchSize.default) +"_" +
                    str(modelParams.learningRate.default) + "_" + str(modelParams.dropoutRate.default) + "_" +
                    str(modelParams.inputSize.default) + "_" + str(modelParams.numLayers.default) + "_" +
                    str(modelParams.hiddenSize.default) + "_" + str(modelParams.outputSize.default) + "_" +
                    str(modelParams.actFnc.default) + "_" + str(ModelParams.dataSet.default) + "_" +
                    str(modelParams.performance.trainLoss) + "_" + str(testLoss) + "_" +
                    str(modelParams.performance.genLoss) +  ".pth")

        torch.save(model.state_dict(),".\Final\%s" %pthName)

        writer.writerow([param.name, modelParams.optimizer.default,
                        modelParams.numEpochs.default, modelParams.BatchSize.default,
                        modelParams.learningRate.default, modelParams.dropoutRate.default,
                        modelParams.inputSize.default, modelParams.numLayers.default,
                        modelParams.hiddenSize.default, modelParams.outputSize.default,
                        modelParams.actFnc.default, ModelParams.dataSet.default,
                        modelParams.performance.trainLoss, testLoss, modelParams.performance.genLoss])
    else:
        writer.writerow(["Final Model", modelParams.optimizer.default, modelParams.numEpochs.default,
                        modelParams.BatchSize.default, modelParams.learningRate.default,
                        modelParams.dropoutRate.default, modelParams.inputSize.default,
                        modelParams.numLayers.default, modelParams.hiddenSize.default,
                        modelParams.outputSize.default, modelParams.actFnc.default,
                        modelParams.performance.trainLoss, testLoss, modelParams.performance.genLoss])

def prepareModel(modelParams):

    # Load training data set
    if(modelParams.dataSet.default == "small"):
         trainSet = pd.read_csv('smallDataset.csv', index_col='timestamp', parse_dates=True)
    elif(modelParams.dataSet.default == "medium"):
        trainSet = pd.read_csv('mediumDataset.csv', index_col='timestamp', parse_dates=True)
    elif(modelParams.dataSet.default == "large"):
        trainSet = pd.read_csv('lorgeDataset.csv', index_col='timestamp', parse_dates=True)

    # Standardize dataset
    ss = StandardScaler()
    dataSS = ss.fit_transform(trainSet)

    # Define forward pass
    model = LSTM(modelParams.outputSize.default, modelParams.inputSize.default,
                 modelParams.hiddenSize.default, modelParams.numLayers.default,
                 modelParams.BatchSize.default, modelParams.dropoutRate.default,
                 modelParams.actFnc.default).to(device)

    # Define backward pass
    lossFnc = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=modelParams.learningRate.default)
    if(modelParams.optimizer.default == "SGD"):
        optimizer = torch.optim.SGD(model.parameters(), lr=modelParams.learningRate.default, momentum=modelParams.momentum.default)

    # Preprocess the data
    samples, targets = splitSequences(dataSS, modelParams.sequenceLength.default)
    targetMMS, mms = scale(targets)
    dataset = Timeseries(samples, np.array(targetMMS))
    trainLoader = DataLoader(dataset, shuffle=True, batch_size=BATCH_SIZE.default)

    return model, lossFnc, optimizer, trainLoader, ss, mms

def findOptimModel(modelParams,  testSet):
    """
    Train, test and save multiple models.

    Args:
        modelParams (ModelParams): Object containing all the model parameters defined in the top.
    Return:
        Saves a CSV file for 8 different models
    """
    mms = MinMaxScaler()

    # Create csv. file name
    csvName = str(modelParams.optimizer.default) + "_" + str(modelParams.numEpochs.default)
    with open(csvName + ".csv", "w", newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["Modified param", "OPTIMIZER", "NUM_EPOCHS", "BATCH_SIZE",
                        "LEARNING_RATE", "DROPOUT_RATE" , "INPUT_SIZE", "NUM_LAYERS",
                        "HIDDEN_SIZE", "OUTPUT_SIZE", "ACTIVATION_FUNCTION" , "DATASET",
                         "trainLoss", "testLoss", "genLoss"])

        # Train n models
        for i in range(MAX_ITERATIONS):
            # Get sequence in which the parameters should be tested
            order = setModSequence(modelParams)
            for param in order:
                print("\n---------- " + param.name + " -----------")
                start_time = time.time()
                paramLoss = []
                loss = 100
                index = -1
                for j in range(len(param.sequence)):
                    start_time2 = time.time()
                    param.default = param.sequence[j]
                    print("\n%s with value:" % param.name, param.default)
                    # Prepare, train, test and save model
                    model, lossFnc, optimizer, trainLoader, ss, mms = prepareModel(modelParams)
                    modelParams.performance.trainLoss = trainModel(model, modelParams.numEpochs.default, trainLoader, optimizer, lossFnc)
                    modelParams.performance.testLoss = testModel(model, testSet, lossFnc, ss, mms, False, modelParams)
                    saveModel(writer, model, modelParams, param, modelParams.performance.testLoss, False)

                    # Append to list of all loss values for the current tested parameter
                    paramLoss.append(modelParams.performance.testLoss)
                    print("Model execution time: %1.10f" % (time.time() - start_time2))

                # Find index of value in list with lowest loss
                for j in range(len(paramLoss)):
                    if(paramLoss[j] < loss):
                        loss = paramLoss[j]
                        index = j
                # update default value of parameter
                param.default = param.sequence[index]
                print("\nParameter execution time: %1.10f" % (time.time() - start_time), "| Lowest testing loss: %1.10f with value:" % loss, param.default)

            # Train final model
            model, lossFnc, optimizer, trainLoader, ss, mms = prepareModel(modelParams)
            modelParams.performance.trainLoss = trainModel(model, modelParams.numEpochs.default, trainLoader, optimizer, lossFnc)
            modelParams.performance.testLoss = testModel(model, testSet, lossFnc, ss, mms, True, modelParams)
            modelParams.performance.genLoss = abs(modelParams.performance.testLoss-modelParams.performance.trainLoss)
            saveModel(writer, model, modelParams, param, modelParams.performance.testLoss, True)

# ----------- Main ---------- #

# Set optimizer and epoch amount
modelParams = ModelParams("Adam", 50)

# Make list of test sets
testSets = []
for i in range(0, TEST_SETS):
    testSets.append(pd.read_csv("TestDataset_%s.csv" % str(i+1), index_col='timestamp', parse_dates=True))

# Find the most optimal hyperparameters for given epoch amount and desired optimizer.
findOptimModel(modelParams, testSets)
